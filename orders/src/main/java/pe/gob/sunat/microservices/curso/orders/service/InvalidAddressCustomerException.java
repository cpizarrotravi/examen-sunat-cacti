package pe.gob.sunat.microservices.curso.orders.service;

public class InvalidAddressCustomerException extends RuntimeException {
  private final String customerId;

  public InvalidAddressCustomerException(String message, String customerId) {
    super(message);
    this.customerId = customerId;
  }

  public String getCustomerId() {
    return customerId;
  }
}
