package pe.gob.sunat.microservices.curso.orders;

import pe.gob.sunat.microservices.curso.orders.service.InvalidAddressCustomerException;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;
import java.util.HashMap;
import java.util.Map;

@Provider
public class InvalidAddressCustomerExceptionMapper implements ExceptionMapper<InvalidAddressCustomerException> {
  @Override
  public Response toResponse(InvalidAddressCustomerException exception) {
    Map data = new HashMap();
    data.put("message", exception.getMessage());
    data.put("id", exception.getCustomerId());
    return Response.status(422).entity(data).type(MediaType.APPLICATION_JSON_TYPE).build();
  }
}
