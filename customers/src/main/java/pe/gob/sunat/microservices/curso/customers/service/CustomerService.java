package pe.gob.sunat.microservices.curso.customers.service;

import pe.gob.sunat.microservices.curso.customers.dao.AddressDaoImpl;
import pe.gob.sunat.microservices.curso.customers.dao.CustomerDaoImpl;
import pe.gob.sunat.microservices.curso.customers.model.Customer;

import java.util.Optional;

/**
 * @author jyauyo
 * 
 * */
public class CustomerService {
	private final CustomerDaoImpl dao;
	private final AddressDaoImpl addressDao;
	private final OrderService orderService;

	public CustomerService(OrderService orderService, CustomerDaoImpl dao, AddressDaoImpl addressDao) {
		this.dao = dao;
		this.addressDao = addressDao;
		this.orderService = orderService;
	}

	public Customer create(Customer customer) {
		return dao.create(customer);
	}

	public Optional<Customer> findById(Long customerId, Boolean includeAddresses) {
		return dao.find(customerId).map(customer -> {
			if (includeAddresses) {
				customer.setAddresses(addressDao.findByCustomer(customerId));
			}
			return customer;
		});
	}

	/**
	 * @author jyauyo
	 * 
	 * @param customerId Long: Id del cliente
	 * @param credentials String: Credenciales del Usuario en sesion
	 * <br>
	 * <br>
	 * Metodo que elimina un cliente siempre y cuando <br>el cliente no tenga pedidos
	 * */
	public void delete(Long customerId, String credentials) {
                System.out.println("invocacion al ms-order "+orderService);
		Boolean tieneOrder = orderService.existeOrderByCustomer(
				customerId, credentials);

                System.out.println("tieneOrder "+tieneOrder.booleanValue());

		if(tieneOrder.booleanValue()){
			throw new InvalidOrderException(
					"No se puede eliminar el cliente debido a que tiene pedidos.",
					customerId.toString());
		}
		dao.delete(customerId);
	}
}
