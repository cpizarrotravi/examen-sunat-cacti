package pe.gob.sunat.microservices.curso.customers.service;

public class InvalidOrderException extends RuntimeException {
  private final String customerId;

  public InvalidOrderException(String message, String customerId) {
    super(message);
    this.customerId = customerId;
  }

  public String getCustomerId() {
    return customerId;
  }
}
