package pe.gob.sunat.microservices.curso.customers.api;

import pe.gob.sunat.microservices.curso.customers.model.Address;
import pe.gob.sunat.microservices.curso.customers.model.Customer;
import pe.gob.sunat.microservices.curso.customers.service.AddressService;
import pe.gob.sunat.microservices.curso.customers.service.CustomerService;

import javax.annotation.security.RolesAllowed;
import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import java.util.List;
import java.util.Optional;

@RolesAllowed("ADMIN")
@Path("/v1/customers")
@Produces(MediaType.APPLICATION_JSON)
public class CustomerResource {
	private final CustomerService customerService;
	private final AddressService addressService;

	public CustomerResource(CustomerService customerService, AddressService addressService) {
		this.customerService = customerService;
		this.addressService = addressService;
	}

	@POST
	public Customer create(@Valid Customer customer) {
		return customerService.create(customer);
	}

	@GET
	@Path("/{id}")
	public Optional<Customer> find(@PathParam("id") Long idCustomer,
			@QueryParam("address") @DefaultValue("false") Boolean includeAddresses) {
		return customerService.findById(idCustomer, includeAddresses);
	}

	/**
	 * @author jyauyo
	 * 
	 * Metodo de eliminacion de Cliente
	 * 
	 */
	@DELETE
	@Path("/{id}")
	public void delete(@PathParam("id") Long idCustomer, @Context HttpHeaders headers) {
		String credentials = headers.getHeaderString("Authorization");
		customerService.delete(idCustomer, credentials);
	}

	@GET
	@Path("/{id}/addresses")
	public List<Address> addressList(@PathParam("id") Long idCustomer) {
		return addressService.addressesByCustomer(idCustomer);
	}

	@POST
	@Path("/{id}/addresses")
	public Address addAddress(@PathParam("id") Long idCustomer, @Valid Address address) {
		return addressService.create(idCustomer, address);
	}

}
